package com.ashish.sampleapp.repositories;

import androidx.lifecycle.MutableLiveData;

import com.ashish.sampleapp.model.UserModel;
import com.ashish.sampleapp.webservice.Api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepository {
    private static UserRepository instance;
    private ArrayList<UserModel> userSet = new ArrayList<>();

    public ArrayList<UserModel> getUserSet() {
        return userSet;
    }

    public static UserRepository getInstance() {
        if (instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    public void fetchUser(OnComplete listener) {
        Api.getInstance().getApiService().getUserList().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res = "";
                    if (response.isSuccessful()) {
                        res = response.body().string();
                    } else {
                        res = response.errorBody().string();
                    }
                    JSONArray resArray = new JSONArray(res);
                    for (int i = 0; i < resArray.length(); i++) {
                        JSONObject eachObj = resArray.getJSONObject(i);
                        userSet.add(new UserModel(
                                eachObj.getString("id"),
                                eachObj.getString("name"),
                                eachObj.getString("email"),
                                eachObj.getString("phone")
                        ));
                    }
                    if (listener !=  null) {
                        listener.onComplete(response.isSuccessful(), userSet);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public interface OnComplete {
        void onComplete(boolean isSuccess, ArrayList<UserModel> userModelArrayList);
    }
}
