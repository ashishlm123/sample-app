package com.ashish.sampleapp.repositories;

import com.ashish.sampleapp.model.PhotoModel;
import com.ashish.sampleapp.model.UserModel;
import com.ashish.sampleapp.webservice.Api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlbumRepository {
    private static AlbumRepository instance;
    private ArrayList<PhotoModel> albumSet = new ArrayList<>();

    public ArrayList<PhotoModel> getAlbumSet() {
        return albumSet;
    }

    public static AlbumRepository getInstance() {
        if (instance == null) {
            instance = new AlbumRepository();
        }
        return new AlbumRepository();
    }

    public void fetchAlbums(String userId, OnComplete listener) {
        Api.getInstance().getApiService().getAlbumList(userId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String res = "";
                    if (response.isSuccessful()) {
                        res = response.body().string();
                    } else {
                        res = response.errorBody().string();
                    }
                    JSONArray resArray = new JSONArray(res);
                    for (int i = 0; i < resArray.length(); i++) {
                        JSONObject eachObj = resArray.getJSONObject(i);
                        PhotoModel photoModel = new PhotoModel();
                        photoModel.setAlbumId(eachObj.getInt("albumId"));
                        photoModel.setPhotoId(eachObj.getInt("id"));
                        photoModel.setPhotoTitle(eachObj.getString("title"));
                        photoModel.setPhotoUrl(eachObj.getString("url"));
                        photoModel.setThumbUrl(eachObj.getString("thumbnailUrl"));
                        albumSet.add(photoModel);
                    }
                    if (listener != null) {
                        listener.onComplete(response.isSuccessful(), albumSet);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public interface OnComplete {
        void onComplete(boolean isSuccess, ArrayList<PhotoModel> userAlbumArrayList);
    }
}

