package com.ashish.sampleapp.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.ashish.sampleapp.R;
import com.ashish.sampleapp.databinding.ActivityUserDetailBinding;
import com.ashish.sampleapp.model.PhotoModel;
import com.squareup.picasso.Picasso;

public class UserDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PhotoModel photo = getIntent().getParcelableExtra("photoModel");

        ActivityUserDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_user_detail);
        binding.setPhoto(photo);


        assert photo != null;
        Picasso.get().load(photo.getPhotoUrl()).into(binding.imgPhoto);

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
