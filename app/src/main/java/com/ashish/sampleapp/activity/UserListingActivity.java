package com.ashish.sampleapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ashish.sampleapp.R;
import com.ashish.sampleapp.adapter.UsersAdapter;
import com.ashish.sampleapp.databinding.ActivityUserListingBinding;
import com.ashish.sampleapp.model.UserModel;
import com.ashish.sampleapp.viewmodel.UserListingViewModel;

public class UserListingActivity extends AppCompatActivity {
    UsersAdapter usersAdapter;
    private UserListingViewModel userListingViewModel;
    private ActivityUserListingBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_listing);

        userListingViewModel = ViewModelProviders.of(this).get(UserListingViewModel.class);

        initRecyclerView();

        userListingViewModel.getUsers().observe(this, userModels -> {
            usersAdapter.notifyDataSetChanged();
        });
        userListingViewModel.isLoading().observe(this, isLoading -> binding.progressBar.setVisibility(
                isLoading ? View.VISIBLE : View.GONE
        ));
    }

    private void initRecyclerView() {
        binding.rvUserList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        binding.rvUserList.setNestedScrollingEnabled(false);
        usersAdapter = new UsersAdapter(userListingViewModel.getUsers().getValue());
        usersAdapter.setClickListener((position, view, object) -> {

            startActivity(new Intent(getApplicationContext(), AlbumListingActivity.class).putExtra("id",
                    ((UserModel) object).getId()));
        });
        binding.rvUserList.setAdapter(usersAdapter);
    }

}

