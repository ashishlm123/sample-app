package com.ashish.sampleapp.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ashish.sampleapp.R;
import com.ashish.sampleapp.adapter.AlbumsAdapter;
import com.ashish.sampleapp.adapter.UsersAdapter;
import com.ashish.sampleapp.databinding.ActivityUserAlbumListingBinding;
import com.ashish.sampleapp.model.PhotoModel;
import com.ashish.sampleapp.viewmodel.AlbumListingViewModel;
import com.ashish.sampleapp.viewmodel.UserListingViewModel;

public class AlbumListingActivity extends AppCompatActivity {
    AlbumsAdapter albumsAdapter;
    private AlbumListingViewModel albumListingViewModel;
    private ActivityUserAlbumListingBinding binding;
    String userId = "";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent != null) {
            userId = intent.getStringExtra("id");
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_album_listing);
        binding.tvAlbumId.setText("Album ID: " + userId);

        albumListingViewModel = ViewModelProviders.of(this).get(AlbumListingViewModel.class);

        initRecyclerView();

        albumListingViewModel.getPhotos(userId).observe(this, albumModels -> {
            albumsAdapter.notifyDataSetChanged();
        });
        albumListingViewModel.isLoading().observe(this, isLoading -> binding.progressBar.setVisibility(
                isLoading ? View.VISIBLE : View.GONE
        ));
    }

    private void initRecyclerView() {
        binding.rvAlbumLists.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        binding.rvAlbumLists.setNestedScrollingEnabled(false);
        albumsAdapter = new AlbumsAdapter(albumListingViewModel.getPhotos(userId).getValue());
        albumsAdapter.setClickListener((position, view, object) -> {

            startActivity(new Intent(getApplicationContext(), UserDetailsActivity.class)
                    .putExtra("photoModel", ((PhotoModel) object)));
        });
        binding.rvAlbumLists.setAdapter(albumsAdapter);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
