package com.ashish.sampleapp.viewmodel;

import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ashish.sampleapp.model.UserModel;
import com.ashish.sampleapp.repositories.UserRepository;
import com.ashish.sampleapp.webservice.Api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserListingViewModel extends ViewModel {
    private MutableLiveData<ArrayList<UserModel>> mUserLists;
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();

    public LiveData<ArrayList<UserModel>> getUsers() {
        if (mUserLists == null) {
            isLoading.setValue(true);
            mUserLists = new MutableLiveData<>();
            UserRepository userRepository = UserRepository.getInstance();
            mUserLists.setValue(userRepository.getUserSet());
            userRepository.fetchUser((isSuccess, userModelArrayList) -> {
                mUserLists.setValue(userModelArrayList);
                isLoading.setValue(false);
            });
        }
        return mUserLists;
    }

    public LiveData<Boolean> isLoading() {
        return isLoading;
    }

}
