package com.ashish.sampleapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ashish.sampleapp.model.PhotoModel;
import com.ashish.sampleapp.repositories.AlbumRepository;
import com.ashish.sampleapp.repositories.UserRepository;

import java.util.ArrayList;

public class AlbumListingViewModel extends ViewModel {
    private MutableLiveData<ArrayList<PhotoModel>> mAlbumLists;
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();

    public LiveData<ArrayList<PhotoModel>> getPhotos(String userId) {
        if (mAlbumLists == null) {
            isLoading.setValue(true);
            mAlbumLists = new MutableLiveData<>();
            AlbumRepository albumRepository = AlbumRepository.getInstance();
            mAlbumLists.setValue(albumRepository.getAlbumSet());
            albumRepository.fetchAlbums(userId, new AlbumRepository.OnComplete() {
                @Override
                public void onComplete(boolean isSuccess, ArrayList<PhotoModel> userAlbumArrayList) {
                    Log.e("albumSize", String.valueOf(userAlbumArrayList.size()));
                    mAlbumLists.setValue(userAlbumArrayList);
                    isLoading.setValue(false);
                }
            });
        }
        return mAlbumLists;
    }
    public LiveData<Boolean> isLoading() {
        return isLoading;
    }
}
