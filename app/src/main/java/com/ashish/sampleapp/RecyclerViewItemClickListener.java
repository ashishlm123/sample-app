package com.ashish.sampleapp;

import android.view.View;

public interface RecyclerViewItemClickListener {
    void onRecyclerViewItemClicked(int position, View view, Object object );
}
