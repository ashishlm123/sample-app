package com.ashish.sampleapp.webservice;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

public class Api {
    private static Api instance;

    static final String usersApi = "users";
    public static final String photosApi = "photos";

    public ApiService getApiService() {
        String host = "https://jsonplaceholder.typicode.com/";
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(host)
                .client(client)
                .build();

        return retrofit.create(ApiService.class);
    }

    public static Api getInstance() {
        if (instance == null) instance = new Api();
        return instance;
    }
}
