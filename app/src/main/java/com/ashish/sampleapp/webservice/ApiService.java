package com.ashish.sampleapp.webservice;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    @GET(Api.usersApi)
    Call<ResponseBody> getUserList();

    @GET(Api.photosApi)
    Call<ResponseBody> getAlbumList(@Query("albumId") String albumId);
}
