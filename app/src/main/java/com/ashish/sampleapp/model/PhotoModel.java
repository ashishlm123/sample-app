package com.ashish.sampleapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PhotoModel implements Parcelable {
    private int albumId;
    private int photoId;
    private String photoTitle;
    private String thumbUrl;
    private String photoUrl;

    public PhotoModel() {
    }

    private PhotoModel(Parcel in) {
        albumId = in.readInt();
        photoId = in.readInt();
        photoTitle = in.readString();
        thumbUrl = in.readString();
        photoUrl = in.readString();
    }

    public static final Creator<PhotoModel> CREATOR = new Creator<PhotoModel>() {
        @Override
        public PhotoModel createFromParcel(Parcel in) {
            return new PhotoModel(in);
        }

        @Override
        public PhotoModel[] newArray(int size) {
            return new PhotoModel[size];
        }
    };

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public String getPhotoTitle() {
        return photoTitle;
    }

    public void setPhotoTitle(String photoTitle) {
        this.photoTitle = photoTitle;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(albumId);
        dest.writeInt(photoId);
        dest.writeString(photoTitle);
        dest.writeString(thumbUrl);
        dest.writeString(photoUrl);
    }
}
